//
//  News.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import Foundation

struct News: Codable {
    
    let status: String
    let totalResults : Int
    var articles: [NewsData]
}

struct NewsData: Codable {
    
    let source: Source?
    let author: String?
    let title: String?
    let description: String?
    let url: URL?
    let urlToImage: URL?
    let publishedAt: String?
    let content: String?
}

struct Source: Codable {
    
    let id: String?
    let name: String?
}
