//
//  ImageCacher.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import UIKit

final class ImageCacher {
    
    static let shared = ImageCacher()
    private init() {}
    
    private var cache = NSCache<NSString, UIImage>()
    
    func cacheImage(from url: URL, completion: (UIImage) -> ()) {
        if let cachedImage = cache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage)
        }
    }
    
    func saveImage(from url: URL, image: UIImage) {
        cache.setObject(image, forKey: url.absoluteString as NSString)
    }
}

