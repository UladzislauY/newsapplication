//
//  ModuleBuilder.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import UIKit

protocol ModuleBuilderProtocol {
    
    func createNewsListScene() -> UIViewController
    func createFavouriteNewsListScene() -> UIViewController
}

final class ModuleBuilder: ModuleBuilderProtocol {
    
     func createFavouriteNewsListScene() -> UIViewController {
        let favouriteNewsListViewScene = FavouriteNewsListViewController()
        let persistance = PersistanceManager()
        let presenter = FavouriteNewsListMainViewPresenter(view: favouriteNewsListViewScene, persistance: persistance)
        favouriteNewsListViewScene.presenter = presenter
        return favouriteNewsListViewScene
    }
    
    func createNewsListScene() -> UIViewController {
        let newsListScene = NewsListViewController()
        let networkManager = NetworkManager()
        let peristance = PersistanceManager()
        let presenter = NewsListViewMainPresenter(view: newsListScene, networkManager: networkManager, persistance: peristance)
        newsListScene.presenter = presenter
        return newsListScene
    }
}
