//
//  Date Extension .swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/29/21.
//

import Foundation

fileprivate let localeIdentifier: String = "en_US_POSIX"
fileprivate let seconds: Int = 0
fileprivate let dateFormat: String = "yyyy-MM-dd"

extension Date {
    
    /**
     Convert corresponding ISO 8601 date format to string
     */
    static func fromISO8601ToString(value: Int?) -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        guard let currentDate = Calendar.current.date(byAdding: .day, value: value ?? .init(), to: date) else {return .init()}
        print(currentDate)
        dateFormatter.locale = Locale(identifier: localeIdentifier)
        dateFormatter.timeZone = TimeZone(secondsFromGMT: seconds)
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: currentDate)
        
    }
    
}

