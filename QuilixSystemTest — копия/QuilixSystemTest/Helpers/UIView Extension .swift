//
//  UIView Extension .swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import UIKit

extension UIView {
    
    func anchorSize(to view: UIView) {
        widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    func activateConstraintsOnView(top: NSLayoutYAxisAnchor?,
                                   leading: NSLayoutXAxisAnchor?,
                                   bottom: NSLayoutYAxisAnchor?,
                                   trailing: NSLayoutXAxisAnchor?,
                                   padding: UIEdgeInsets = .zero,
                                   size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
            
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: padding.right).isActive = true
        }
        
        if size.width != 0 {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0 {
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
        
    }
    
    func pinShadow(to view: UIView) {
        view.layer.masksToBounds = true
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 10, height: 20)
        self.layer.shadowRadius = 30
        self.layer.shadowColor = UIColor(red: 0.802, green: 0.802, blue: 0.881, alpha: 0.54).cgColor
        self.layer.masksToBounds = false
        
    }
    func pinGradient(to view: UIView) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = [
            UIColor(red: 0.933, green: 0.929, blue: 0.945, alpha: 1).cgColor,
            UIColor(red: 0.96, green: 0.955, blue: 0.975, alpha: 1).cgColor
        ]
        
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer.endPoint = CGPoint(x: 0.75, y: 0.5)
        
        layer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
        
        layer.bounds = view.bounds.insetBy(dx: -1 * view.bounds.size.width, dy: -1 * view.bounds.size.height)
        layer.position = view.center
        self.layer.insertSublayer(layer, at: 0)
        
        let layer2 = CAGradientLayer()
        layer2.frame = self.bounds
        layer2.colors = [
            UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor,
            UIColor(red: 1, green: 1, blue: 1, alpha: 0).cgColor
        ]
        
        layer2.locations = [0, 1]
        layer2.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer2.endPoint = CGPoint(x: 0.75, y: 0.5)
        
        layer2.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
        
        layer2.bounds = view.bounds.insetBy(dx: -1 * view.bounds.size.width, dy: -1 * view.bounds.size.height)
        layer2.position = view.center
        layer2.cornerRadius = 13
        self.layer.insertSublayer(layer2, at: 1)
    }
}
