//
//  UIImageView Extension .swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import UIKit

fileprivate enum ImageConstant {
    
    static var imageURL: URL?
    static let placeholderImage: String = "placeholderImage"
}

extension UIImageView {
    
    /**
     Load image from given URL and instantly cache it
     */
    func loadImage(from url: URL) {
        ImageConstant.imageURL = url
        image = nil
        ImageCacher.shared.cacheImage(from: url) { (cachedImage) in
            self.image = cachedImage
        }
        DispatchQueue.global(qos: .utility).async {
            guard let data = try? Data(contentsOf: url) else {return}
            guard let imageToCache = UIImage(data: data) else {return}
            DispatchQueue.main.async {
                if ImageConstant.imageURL == url {
                    self.image = imageToCache
                } else if self.image == nil {
                    self.image = UIImage(named: ImageConstant.placeholderImage)
                }
            }
            ImageCacher.shared.saveImage(from: url, image: imageToCache)
        }
    }
}
