//
//  UIColor Extension.swift
//  QuilixSystemTest
//
//  Created by Владислав on 1/10/22.
//

import UIKit

enum ColorSet {
    case cellBorderColor
}

extension UIColor {
    
    static func setCustomColor(with color: ColorSet) -> UIColor? {
        switch color {
        
        case .cellBorderColor:
            return UIColor(red: 0.342, green: 0.339, blue: 0.339, alpha: 0.28)
        }
    }
}
