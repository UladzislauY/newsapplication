//
//  NetworkManager.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import Foundation

protocol Networking {
    
    func retrieveNews(from url: URL?, completion: @escaping (News?) -> ())
}

final class NetworkManager: Networking {
    
    func retrieveNews(from url: URL?, completion: @escaping (News?) -> ()) {
        guard let url = url else {return}
        guard let task = self.createDataTask(with: url, completion: completion) else {return}
        task.resume()
    }
    
    private func createDataTask(with url: URL, completion: @escaping (News?) -> ()) -> URLSessionDataTask? {
        return URLSession.shared.dataTask(with: url) { (data, _, _) in
            guard let data = data else {return}
            let news: News? = self.decodeObject(from: data, object: News.self)
            DispatchQueue.main.async {
                completion(news)
            }
        }
    }
    
    private func decodeObject<T: Decodable>(from data: Data?, object: T.Type) -> T? {
        guard let data = data else {return nil}
        let decoder = JSONDecoder()
        let object = try? decoder.decode(object.self, from: data)
        return object
    }
}
