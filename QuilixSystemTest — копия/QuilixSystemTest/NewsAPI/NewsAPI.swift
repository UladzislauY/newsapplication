//
//  NewsAPI.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/30/21.
//

import Foundation

fileprivate enum URLComponent {
    
    static let scheme: String = "https"
    static let host: String = "newsapi.org"
}

enum QueryItem: String, Hashable {
    
    case query
    case apiKey
    case fromDate
    case toDate
    case pageSize
    case page
    
    var rawValue: String {
        switch self {
        case .query:
            return "q"
        case .apiKey:
            return "apiKey"
        case .fromDate:
            return "from"
        case .toDate:
            return "to"
        case .pageSize:
            return "pageSize"
        case .page:
            return "page"
        }
    }
}

enum NewsAPI {
    
    case everything(queryItems: [QueryItem: String])
    case topHeadlines
    
    var url: URL? {
        var component = URLComponents()
        component.scheme = URLComponent.scheme
        component.host = URLComponent.host
        component.path = path
        component.queryItems = queryItems()
        return component.url
    }
    
    fileprivate func queryItems() -> [URLQueryItem]? {
        var queryItemsList = [URLQueryItem]()
        switch self {
        
        case .everything(let queryItems):
            for(key, value) in queryItems {
                queryItemsList.append(contentsOf: [URLQueryItem(name: key.rawValue, value: value), URLQueryItem(name: key.rawValue, value: value), URLQueryItem(name: key.rawValue, value: value), URLQueryItem(name: key.rawValue, value: value), URLQueryItem(name: key.rawValue, value: value)])
            }
        case .topHeadlines:
            return nil
        }
        
        return queryItemsList
    }
}

extension NewsAPI {
    
    fileprivate var path: String {
        switch self {
        case .everything:
            return "/v2/everything"
        case .topHeadlines:
            return "/v2/top-headlines/sources"
        }
    }
}
