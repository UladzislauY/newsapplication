//
//  PersistanceManager .swift
//  QuilixSystemTest
//
//  Created by Владислав on 1/11/22.
//

import Foundation

protocol Persisting {
    
    func saveLikedNews(news: NewsData)
    func retrieveLikeNews() -> [NewsData]?
}

class PersistanceManager: Persisting {
    
    //MARK: - Coding Keys
    private enum Keys: String {
        case likedNews = "likedNews"
        
        var stringRepresentation: String {
            rawValue
        }
    }
    
    private var persistance = UserDefaults.standard
    private let decoder = JSONDecoder()
    private let encoder = JSONEncoder()
    
    
    //MARK: - Persistance Methods
    
    func saveLikedNews(news: NewsData) {
        var likedNews = [NewsData]()
        if let retrievedNews = self.retrieveLikeNews() {likedNews = retrievedNews}
        likedNews.append(news)
        let encodedNews = likedNews.compactMap({try? encoder.encode($0)})
        persistance.setValue(encodedNews, forKey: Keys.likedNews.rawValue)
    }
    
    func retrieveLikeNews() -> [NewsData]? {
        guard let decodedData = persistance.array(forKey: Keys.likedNews.rawValue) else {return []}
        let likedNews = decodedData.compactMap({try? decoder.decode(NewsData.self, from: $0 as! Data)})
        return likedNews
    }
}
