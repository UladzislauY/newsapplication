//
//  FavouriteNewsListViewController.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/31/21.
//

import UIKit

final class FavouriteNewsListViewController: UIViewController {
    
    //MARK: - Constants
    private enum Constant {
        static let heightForTableViewRow: CGFloat = 300
        static let defaultSize: CGFloat = 0
        static let title: String = "Favourite"
    }
    //MARK: - UserInterface Elements
    private lazy var favouriteNewsTableView = NewsListTableView(frame: .zero, dataSource: self, delegate: self, reusableCell: FavouriteNewsListTableViewCell.self, reuseIdentifier: FavouriteNewsListTableViewCell.reuseIdentifier)
    //MARK: - Variables
    public var presenter: FavouriteNewsListViewPresenterProtocol!
    //MARK: - ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Constant.title
        view.backgroundColor = .white
        view.addSubview(favouriteNewsTableView)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.favouriteNewsTableView.activateConstraintsOnView(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: Constant.defaultSize, left: Constant.defaultSize, bottom: Constant.defaultSize, right: Constant.defaultSize), size: .init())
    }
}
//MARK: - UITableView Protocols
extension FavouriteNewsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constant.heightForTableViewRow
    }
}

extension FavouriteNewsListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.favouriteNews
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let favouriteNewsCell = tableView.dequeueReusableCell(withIdentifier: FavouriteNewsListTableViewCell.reuseIdentifier, for: indexPath) as? FavouriteNewsListTableViewCell else {return UITableViewCell()}
        favouriteNewsCell.populateFavouriteNewsListTableViewCell(with: presenter.getSpecificLikedNews(at: indexPath.row))
        return favouriteNewsCell
    }
}

//MARK: - View Presenter Protocol Extension
extension FavouriteNewsListViewController: FavouriteNewsListViewProtocol {
}
