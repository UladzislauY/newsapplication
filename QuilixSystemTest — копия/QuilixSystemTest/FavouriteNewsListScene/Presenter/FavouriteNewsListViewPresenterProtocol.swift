//
//  FavouriteNewsListViewPresenterProtocol.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/31/21.
//

import Foundation

protocol FavouriteNewsListViewPresenterProtocol {
    
    func retrieveLikedNews() -> [NewsData]?
    func getSpecificLikedNews(at index: Int) -> NewsData?
    var favouriteNews: Int {get}
}
