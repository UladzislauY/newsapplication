//
//  FavouriteNewsListMainViewPresenter.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/31/21.
//

import Foundation

final class FavouriteNewsListMainViewPresenter: FavouriteNewsListViewPresenterProtocol {
    
    public  var favouriteNews: Int {
        return retrieveLikedNews()?.count ?? 0
    }
    //MARK: - Variables
    private weak var view: FavouriteNewsListViewProtocol?
    private var persistance: Persisting
    //MARK: - Initializer
    init(view: FavouriteNewsListViewProtocol, persistance: Persisting) {
        self.view = view
        self.persistance = persistance
    }
    
    func retrieveLikedNews() -> [NewsData]? {
        return persistance.retrieveLikeNews()
    }
    
    func getSpecificLikedNews(at index: Int) -> NewsData? {
        return retrieveLikedNews()?[index]
    }
}
