//
//  FavouriteNewsListTableViewCell.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/31/21.
//

import UIKit

fileprivate protocol FavouriteNewsListTableViewCellDelegate {
    func populateFavouriteNewsListTableViewCell(with model: NewsData?)
}

final class FavouriteNewsListTableViewCell: UITableViewCell {
    //MARK: - Constant
    private enum Constant {
        
        static let newsTitleLabelFontSize: CGFloat = 18
        static let newsTitleLabelFontName: String = "Helvetica"
        static let newsDescriptionLabelFontSize: CGFloat = 15
        static let newsDescriptionLabelFontName: String = "Helvetica"
        static let defaultValue: CGFloat = 0
        static let newsImageViewWidth: CGFloat = 200
        static let newsTitleLabelTopAnchor: CGFloat = 10
        static let newsTitleLabelLeftAnchor: CGFloat = 10
        static let newsTitleLabelRightAnchor: CGFloat = -10
        static let newsTitleLabelHeight: CGFloat = 60
        static let newsDescriptionLabelTopAnchor: CGFloat = 30
        static let newsDescriptionLabelLeftAnchor: CGFloat = 15
        static let newsDescriptionLabelRightAnchor: CGFloat = -15
        static let newsDescriptionLabelBottomAnchor: CGFloat = -30
        
    }
    //MARK: - UserInterface Elements
    private lazy var newsTitleLabel = ReusableLabel(font: UIFont(name: Constant.newsTitleLabelFontName, size: Constant.newsTitleLabelFontSize) ?? .init(), textColor: .black, alignment: .center)
    private lazy var newsDescriptionLabel = ReusableLabel(font: UIFont(name: Constant.newsDescriptionLabelFontName, size: Constant.newsDescriptionLabelFontSize) ?? .init(), textColor: .black, alignment: .right)
    private lazy var newsImageView: UIImageView = {
        let newsImage = UIImageView()
        newsImage.contentMode = .scaleToFill
        return newsImage
    }()
    //MARK: - Variables
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    //MARK: - Initializers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    //MARK: - Cell Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //MARK: - Private Methods
    private func setup() {
        
        contentView.addSubview(newsImageView)
        contentView.addSubview(newsTitleLabel)
        contentView.addSubview(newsDescriptionLabel)
        
        newsImageView.activateConstraintsOnView(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: nil, padding: .init(top: Constant.defaultValue, left: Constant.defaultValue, bottom: Constant.defaultValue, right: Constant.defaultValue), size: .init(width: Constant.newsImageViewWidth, height: Constant.defaultValue))
        
        newsTitleLabel.activateConstraintsOnView(top: contentView.topAnchor, leading: newsImageView.trailingAnchor, bottom: nil, trailing: contentView.trailingAnchor, padding: .init(top: Constant.newsTitleLabelTopAnchor, left: Constant.newsTitleLabelLeftAnchor, bottom: Constant.defaultValue, right: Constant.newsTitleLabelRightAnchor), size: .init(width: Constant.defaultValue, height: Constant.newsTitleLabelHeight))
        
        newsDescriptionLabel.activateConstraintsOnView(top: newsTitleLabel.bottomAnchor, leading: newsImageView.trailingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor, padding: .init(top: Constant.newsDescriptionLabelTopAnchor, left: Constant.newsDescriptionLabelLeftAnchor, bottom: Constant.newsDescriptionLabelBottomAnchor, right: Constant.newsDescriptionLabelRightAnchor), size: .init())
    }
}
//MARK: - Cell Protocol Extension
extension FavouriteNewsListTableViewCell: FavouriteNewsListTableViewCellDelegate {
    func populateFavouriteNewsListTableViewCell(with model: NewsData?) {
        newsTitleLabel.text = model?.title
        newsDescriptionLabel.text = model?.description
        if let imageURL = model?.urlToImage {
            newsImageView.loadImage(from: imageURL)
        }
    }
}
