//
//  NewsListTableViewCell.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import UIKit

fileprivate protocol NewsListTableViewCellDelegate {
    
    func populateNewsListTableViewCell(with model: NewsData?)
    func expandDescriptionLabel()
}

class NewsListTableViewCell: UITableViewCell {
    
    //MARK: - Constants
    private enum Constants {
        
        static let numberOfLines: Int = 3
        static let titleLabelFontName: String = "Helvetica"
        static let titleLabelFontSize: CGFloat = 20
        static let titleLabelTopAnchor: CGFloat = 15
        static let titleLabelLeftAnchor: CGFloat = 20
        static let titleLabelRightAnchor: CGFloat = -20
        static let titleLabelHeight: CGFloat = 30
        static let descriptionLabelFontName: String = "Helvetica"
        static let descriptionLabelFontSize: CGFloat = 14
        static let newsImageViewCornerRadius: CGFloat = 12
        static let topInset: CGFloat = 20
        static let leftInset: CGFloat = 20
        static let rightInset: CGFloat = 20
        static let bottomInset: CGFloat = 20
        static let defaultValue: CGFloat = 0
        static let newsImageViewHeight: CGFloat = 300
        static let newsImageViewRightAnchor: CGFloat = -20
        static let descriptionLabelTopAnchor: CGFloat = 10
        static let descriptionLabelLeftAnchor: CGFloat = 30
        static let descriptionLabelRightAnchor: CGFloat = -30
        static let descriptionLabelBottomAnchor: CGFloat = -30
        static let cellCornerRadius: CGFloat = 12
        static let cellBorderWidth: CGFloat = 0.6
        static let duration: Double = 0.3
        
    }
    //MARK: - UserInterface Elements
    private lazy var newsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = Constants.newsImageViewCornerRadius
        return imageView
    }()
    
    private lazy var titleLable = ReusableLabel(font: UIFont(name: Constants.titleLabelFontName, size: Constants.titleLabelFontSize) ?? .init(), textColor: .black, alignment: .left)
    private lazy var descriptionLabel = ReusableLabel(font: UIFont(name: Constants.descriptionLabelFontName, size: Constants.descriptionLabelFontSize) ?? .init(), textColor: .black, alignment: .left)
    private lazy var subview: UIView = UIView()
    //MARK: - Variables
    public class var reuseIdentifier: String {
        return String(describing: self)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    //MARK: - Cell Life Cycle Methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView?.image = nil
        self.descriptionLabel.text = nil
        self.titleLable.text = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    //MARK: - Private methods
    private func setup() {
        contentView.addSubview(subview)
        subview.addSubview(titleLable)
        subview.addSubview(newsImageView)
        subview.addSubview(descriptionLabel)
        
        self.subview.activateConstraintsOnView(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor)

        self.titleLable.activateConstraintsOnView(top: subview.topAnchor, leading: subview.leadingAnchor, bottom: nil, trailing: subview.trailingAnchor, padding: .init(top: Constants.titleLabelTopAnchor, left: Constants.titleLabelLeftAnchor, bottom: Constants.defaultValue, right: Constants.titleLabelRightAnchor), size: .init(width: Constants.defaultValue, height: Constants.titleLabelHeight))
        
        self.newsImageView.activateConstraintsOnView(top: titleLable.bottomAnchor, leading: subview.leadingAnchor, bottom: nil, trailing: subview.trailingAnchor, padding: .init(top: Constants.topInset, left: Constants.leftInset, bottom: Constants.defaultValue, right: Constants.newsImageViewRightAnchor), size: .init(width: Constants.defaultValue, height: Constants.newsImageViewHeight))
        
        self.descriptionLabel.activateConstraintsOnView(top: newsImageView.bottomAnchor, leading: subview.leadingAnchor, bottom: subview.bottomAnchor, trailing: subview.trailingAnchor, padding: .init(top: Constants.descriptionLabelTopAnchor, left: Constants.descriptionLabelLeftAnchor, bottom: Constants.descriptionLabelBottomAnchor, right: Constants.descriptionLabelRightAnchor), size: .init())

        self.layer.cornerRadius = Constants.cellCornerRadius
        self.layer.borderWidth = Constants.cellBorderWidth
        self.layer.borderColor = UIColor.setCustomColor(with: .cellBorderColor)?.cgColor
        self.backgroundColor = #colorLiteral(red: 0.4982970953, green: 0.5742670894, blue: 0.8687684536, alpha: 1)
        self.pinShadow(to: subview)
    }
    
    @objc private func showMoreText(gesture: UIGestureRecognizer) {
        if let expandableLabel = gesture.view as? ReusableLabel {
            expandableLabel.numberOfLines = expandableLabel.numberOfLines == Constants.numberOfLines ? 0 : Constants.numberOfLines
            UIView.animate(withDuration: Constants.duration) {
                expandableLabel.superview?.layoutIfNeeded()
            }
        }
    }
}
//MARK: - Protocol Extension
extension NewsListTableViewCell: NewsListTableViewCellDelegate {
    func expandDescriptionLabel() {
        let tapGesture: UIGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showMoreText(gesture:)))
        descriptionLabel.addGestureRecognizer(tapGesture)
    }
    
    func populateNewsListTableViewCell(with model: NewsData?) {
        guard let news = model else {return}
        self.titleLable.text = news.title
        self.descriptionLabel.text = news.description
        guard let imageURL = news.urlToImage else {return}
        self.newsImageView.loadImage(from: imageURL)
    }
}
