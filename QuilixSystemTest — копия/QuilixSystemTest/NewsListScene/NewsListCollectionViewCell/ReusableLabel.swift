//
//  ReusableLabel.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import UIKit

final class ReusableLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(font: UIFont, textColor: UIColor, alignment: NSTextAlignment) {
        self.init()
        super.font = font
        super.textColor = textColor
        super.textAlignment = alignment
        super.numberOfLines = 3
        super.isUserInteractionEnabled = true 
    }
}
