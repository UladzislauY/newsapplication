//
//  NewsListViewProtocol.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import Foundation

protocol NewsListViewProtocol: AnyObject {
    
    func retreiveNewsForPastTwentyFourHours(isPaginating: Bool, fromDate: String)
    func reloadData()
    func customizeNavigationBar()
}
