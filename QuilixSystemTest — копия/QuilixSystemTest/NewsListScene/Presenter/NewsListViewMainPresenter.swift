//
//  NewsListViewMainPresenter.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
// 3cbd6b67245d476788b35f25132f41b1
// 85e75d24c0144d24a684ba38cf97fb1c
// 8767994e7e4f412db45375a5f3884808
// 3413120c2d9d4ff788b3084fbd823908

import Foundation

final class NewsListViewMainPresenter: NewsListViewPresenterProtocol {
    
    //MARK: - Constants
    private enum Constant {
        static let apiKey: String = "8767994e7e4f412db45375a5f3884808"
    }
    //MARK: -  Public Variables
    public var numberOfDays: Int = 0
    public var isPaginationOn: Bool = false
    public var news: News? = News(status: "", totalResults: 0, articles: [])
    public var filteredNewsByTitle: [NewsData] = []
    //MARK: - Private Variables
    weak private var view: NewsListViewProtocol?
    private let networkManager: Networking
    private var persistance: Persisting
    private var searchResult = String()
    private var isFiltering: Bool = false
    private var originalData: [NewsData] = []
    private var searchQueries = ["France", "Russia", "Sport", "Food", "UK", "USA", "Italy", "Football", "Apple", "Google", "Computers", "Information", "Wars", "Games", "Bitcoin", "Books"]
    //MARK: - Initializer
    
    init(view: NewsListViewProtocol, networkManager: Networking, persistance: Persisting) {
        self.view = view
        self.networkManager = networkManager
        self.persistance = persistance
    }
    
    //MARK: - Presenter Protocol Methods
    
    func getSpecificNews(at index: Int) -> NewsData? {
        guard let news = news?.articles else {return nil}
        if isFiltering == true {
            return originalData[index]
        }
        return news[index]
    }
    
    func requestNewsForTwentyFourHours(isPaginating: Bool,fromDate: String, completion: @escaping (News) ->()) {
        guard let searchQuery = searchQueries.randomElement() else {return}
        guard let twentyFourHoursNewsURL = NewsAPI.everything(queryItems: [.query: searchQuery, .fromDate: fromDate,  .apiKey: Constant.apiKey]).url else {return}
        
        self.networkManager.retrieveNews(from: twentyFourHoursNewsURL) { (news) in
            guard let news = news else {return}
            if isPaginating == false {
                completion(news)
                self.isPaginationOn = false
                
            }
            
            else if isPaginating == true {
                self.news?.articles.append(contentsOf: news.articles)
                self.isPaginationOn = false
                
            }
        }
    }
    
    func sortNewsByTitle(searchText: String) {
        originalData = filteredNewsByTitle.filter({ (newsData) -> Bool in
            return (newsData.title?.contains(searchText) ?? .init())
        })
        self.searchResult = searchText
    }
    
    func getNewsCountIfFiltering() -> Int {
        if isFiltering == true {
            return originalData.count
        }
        return news?.articles.count ?? .zero
    }
    
    func saveLikedNews(title: String?, description: String?, image: URL?) {
        persistance.saveLikedNews(news: NewsData(source: nil, author: nil, title: title, description: description, url: nil, urlToImage: image, publishedAt: nil, content: nil))
    }
}
