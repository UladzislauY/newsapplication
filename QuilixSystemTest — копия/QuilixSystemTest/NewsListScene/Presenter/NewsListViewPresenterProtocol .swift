//
//  NewsListViewPresenterProtocol .swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import Foundation

protocol NewsListViewPresenterProtocol {
    
    func saveLikedNews(title: String?, description: String?, image: URL?)
    func getNewsCountIfFiltering() -> Int
    func sortNewsByTitle(searchText: String)
    func requestNewsForTwentyFourHours(isPaginating: Bool, fromDate: String, completion: @escaping (News) ->())
    func getSpecificNews(at index: Int) -> NewsData?
    var news: News? {get set}
    var numberOfDays: Int {get set}
    var isPaginationOn: Bool {get set}
    var filteredNewsByTitle: [NewsData] {get set}
    

}
