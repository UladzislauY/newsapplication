//
//  NewsListTableView.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import UIKit

final class NewsListTableView: UITableView {
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        super.backgroundColor = .white
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(frame: CGRect, dataSource: UITableViewDataSource, delegate: UITableViewDelegate, reusableCell: UITableViewCell.Type, reuseIdentifier: String) {
        self.init(frame: frame)
        super.dataSource = dataSource
        super.delegate = delegate
        super.register(reusableCell, forCellReuseIdentifier: reuseIdentifier)
        super.separatorStyle = .none
        
    }
}
