//
//  ViewController.swift
//  QuilixSystemTest
//
//  Created by Владислав on 12/28/21.
//

import UIKit

final class NewsListViewController: UIViewController {
    
    //MARK: - Constants
    private enum Constant {
        static let favourite: String = "favourite"
        static let searchBarPlaceholder: String = "Search for news"
        static let searchBarHeight: CGFloat = 60
        static let defaultValue: CGFloat = 0
        static let searchBarLeftAnchor: CGFloat = 5
        static let searchBarRightAnchor: CGFloat = -5
        static let heightForRow: CGFloat = 500
        static let tableViewFooterHeight: CGFloat = 10
        static let tableViewContentSizeSpacing: CGFloat = 100
        
    }
    //MARK: - Variables
    public var presenter: NewsListViewPresenterProtocol!
    //MARK: - UserInterface Elements
    private lazy var searchBar: UISearchBar = {
        let search = UISearchBar()
        search.delegate = self
        search.placeholder = Constant.searchBarPlaceholder
        return search
    }()
    private lazy var newsListTableView = NewsListTableView(frame: .zero, dataSource: self, delegate: self, reusableCell: NewsListTableViewCell.self, reuseIdentifier: NewsListTableViewCell.reuseIdentifier)
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    private var viewContainer: [UIView] {return [searchBar, newsListTableView]}
    //MARK: - ViewController Life Cycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.viewContainer.forEach({view.addSubview($0)})
        self.retreiveNewsForPastTwentyFourHours(isPaginating: false, fromDate: Date.fromISO8601ToString(value: nil))
        self.customizeNavigationBar()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        searchBar.activateConstraintsOnView(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: Constant.defaultValue, left: Constant.searchBarLeftAnchor, bottom: Constant.defaultValue, right: Constant.searchBarRightAnchor), size: .init(width: Constant.defaultValue, height: Constant.searchBarHeight))
        
        newsListTableView.activateConstraintsOnView(top: searchBar.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: Constant.defaultValue, left: Constant.defaultValue, bottom: Constant.defaultValue, right: Constant.defaultValue), size: .init())
    }
    
    @objc private func pushToFavouriteScene() {
        self.navigationController?.pushViewController(ModuleBuilder().createFavouriteNewsListScene(), animated: true)
    }
}
//MARK: - Protocol Extension
extension NewsListViewController: NewsListViewProtocol {
    
    func customizeNavigationBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: Constant.favourite), style: .done, target: self, action: #selector(pushToFavouriteScene))
    }
    
    func retreiveNewsForPastTwentyFourHours(isPaginating: Bool, fromDate: String) {
        self.presenter.requestNewsForTwentyFourHours(isPaginating: isPaginating, fromDate: fromDate) {  [weak self] (news) in
            self?.presenter.news?.articles.append(contentsOf: news.articles)
            self?.presenter.filteredNewsByTitle = news.articles
            self?.reloadData()
        }
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.newsListTableView.reloadData()
        }
    }
}
//MARK: - UISearchBar Protocol
extension NewsListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter.sortNewsByTitle(searchText: searchText)
        self.reloadData()
    }
}
//MARK: - UITableView Protocols
extension NewsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard let likedNews = presenter.getSpecificNews(at: indexPath.row) else {return}
        presenter.saveLikedNews(title: likedNews.title, description: likedNews.description, image: likedNews.urlToImage)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constant.heightForRow
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let positionY = scrollView.contentOffset.y
        if positionY > newsListTableView.contentSize.height - Constant.tableViewContentSizeSpacing - scrollView.frame.size.height {
            guard !presenter.isPaginationOn else {return}
            presenter.numberOfDays -= 1
            self.retreiveNewsForPastTwentyFourHours(isPaginating: true, fromDate: Date.fromISO8601ToString(value: presenter.numberOfDays))
            self.reloadData()
            guard presenter.isPaginationOn else {return}
        }
    }
}

extension NewsListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getNewsCountIfFiltering()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let newsListCell = tableView.dequeueReusableCell(withIdentifier: NewsListTableViewCell.reuseIdentifier, for: indexPath) as? NewsListTableViewCell else {return UITableViewCell()}
        newsListCell.populateNewsListTableViewCell(with: presenter.getSpecificNews(at: indexPath.row))
        newsListCell.expandDescriptionLabel()
        return newsListCell
    }
}
